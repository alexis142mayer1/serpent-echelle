/*Nom programmeur: Alexis Mayer*/
/*Matricule programmeur: 2668264*/
/*Nom fichier: main.js*/
/*Description: Fichier principal pour le jeux Serpent Échelle programmer avec des fonctions.
               Les fonctions alternent entre joueur #1 et #2 pour bouger dans le tableau de couleur en html.*/

//Initialise tout les boutons html pour assigner attributs et fonctions.
let btnCommencer = document.getElementById('Commencer');
let btnRouller = document.getElementById('Rouller');
let btnProchain = document.getElementById('Prochain');

//Initialise tout les case de texte pour annoncer changement et # du dés.
let txtDetail = document.getElementById('Detail');
let txtnumeros = document.getElementById('NumDes');

//Contient un string soit du joueur #1 (P1) ou joueur #2 (P2).
let joueurActif = 0;

//Contient le numéros du dés roullé.
let nombreDes = 0;

//Contient les indexs (int) de chaque joueur.
let indexP1 = 0;
let indexP2 = 0;

//Représentation en array du tableau en html. Il contient les serpents et échelles qui affecterons les joueurs.
let Board = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

//Fonction utilisé pour ajouter des détails selon la partie jouer. 
let Annoncer = function(message) {
    txtDetail.textContent = message;
}

//Fonction utilisé au début pour commencer la rotation de joueur. Elle est appelée par btnCommencer. 
let CommencerPartie = function(){
    //Recommence tout les variables a la valeur par défaut.
    reset();
    //Assigne un joueur actif au hasard.
    joueurActif = JoueurAleatoire();
    //Assigne des serpents et échelles au hasard.
    SEAleatoire();
    //Commence le tour d'une partie.
    tour();
}

//Ajouter Event Listener pour quand le joueur click le bouton pour rouller le dés. 
btnRouller.addEventListener('click', function () {
    
    //Assigne un nombre aléatoire entre 1 - 6 pour le dés roullé.
    nombreDes = DesAleatoire();
    txtnumeros.textContent = nombreDes;

    //Affecte la disponibilité pour chacun des boutons. 
    //De cette façons, bloque au joueur de rouller un dés plusieurs fois dans un tour. 
    btnRouller.disabled = true;
    btnProchain.disabled = false;

    //Complète le déplacement du joueur dans le tableau html.
    AvancerJoueurActif();
});

//Fonction utilisé pour controler les options du joueur actif et indiquer lequel doit jouer.
let tour = function() {
    btnRouller.disabled = false;

    if(joueurActif == 'P1'){
        Annoncer('Joueur #1, rouller votre dés!');
    } else{
        Annoncer('Joueur #2, rouller votre dés!');
    }
}

//Fonction utilisé pour choisir une case aleatoire entre 1 et 19.
let CaseAleatoire = function(){
    let nombreCaseAleatoire = Math.random() * 19 + 1;
    nombreCaseAleatoire = Math.floor(nombreCaseAleatoire);
    return nombreCaseAleatoire;
}

//Fonction utilisé pour generer un nombre aleatoire entre 1 et 6.
let DesAleatoire = function(){
    let Des = Math.random() * 6 + 1;
    Des = Math.floor(Des);
    return Des;
}

//Fonction utilisé au debut pour choisir un joueur au hasard pour commencer une partie.
let JoueurAleatoire = function(){

    let joueur = Math.random() * 2 + 1;
    joueur = Math.floor(joueur);

    if(joueur == 1){
        return 'P1';
    } else {
        return 'P2';
    }
}

//Fonction utilisé pour alterner entre le joueur #1 et #2. Elle est appelé par btnProchain.
let Prochain = function(){
    if(joueurActif == 'P1'){
        joueurActif = 'P2';
    } else {
        joueurActif = 'P1';
    }

    btnProchain.disabled = true;
    tour();
}

//Fonction utilisé pour recommencer tout les tableaux, variables et images d'une partie. 
let reset = function(){

    if(indexP1 != 0){
        EffacerImg(indexP1);
    } 
    
    indexP1 = 0;
    
    if(indexP2 != 0){
        EffacerImg(indexP2);
    }
    
    indexP2 = 0;

    for(let index = 1;index < 20;index++){
        document.getElementById('C' + index).style.backgroundColor = 'White';
        document.getElementById('C' + index).innerText = index;
    }
    
    btnProchain.disabled = true;
    btnRouller.disabled = true;
    Board = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    nombreDes = 0;
    txtnumeros.textContent = 1;
}

//Fonction utilisé pour effacer image du joueur en paramètre.
let EffacerImg = function(indexJoueur) {

    document.getElementById('C' + indexJoueur).innerHTML = "<img src=''>";
    document.getElementById('C' + indexJoueur).innerText = indexJoueur;

    if(joueurActif == 'P1'){
        if(indexJoueur == indexP2){
            document.getElementById('C' + indexJoueur).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        }
    } else {
        if(indexJoueur == indexP1){
            document.getElementById('C' + indexJoueur).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        }
    }
}

//Fonction utilisé pour attribuer 3 serpents et echelles aleatoire dans le tableau Board[].
let SEAleatoire = function(){

    //Boucle For different pour les echelles et serpents.
    for(let nombreEchelle = 3; nombreEchelle > 0; nombreEchelle--){

        //Représente les cases de échelle ou serpent. 
        let Case1 = CaseAleatoire();
        let Case2 = Case1 + DesAleatoire();

        //Représente les couleurs assigner au cases.
        let CouleurCourante = 0;
        let Couleur1 = 'Blue';
        let Couleur2 = 'Pink';
        let Couleur3 = 'Yellow';

        //Selon la boucle For, la couleur change pour les trois différents serpents ou échelles.
        if(nombreEchelle == 3){
            CouleurCourante = Couleur3;
        } else if(nombreEchelle == 2){
            CouleurCourante = Couleur2;
        } else {
            CouleurCourante = Couleur1;
        }

        //Assigne une valeur de Case2 pour quelle se situe entre 2 et 18.
        while(Case2 > 19){
            Case2 = Case1 + DesAleatoire();
        }

        //Si les cases choisis sont vide, assigne la couleur dans la variable couleurcourante. Sinon, ont ajoute une nouvelle boucle pour essayer encore.
        if((Board[Case1] == '') && (Board[Case2] == '')){
            //Le 'E' représente 'Échelle' et 'S' représente 'Serpent'. Aussi, le 'D' représente Début et 'F' représente 'Fin'.
            Board[Case1] = 'ED' + nombreEchelle;
            Board[Case2] = 'EF' + nombreEchelle;
            document.getElementById('C' + Case1).style.backgroundColor = CouleurCourante;
            document.getElementById('C' + Case2).style.backgroundColor = CouleurCourante;
        
        } else {
            nombreEchelle++;
            continue;
        }
    }
    
    for(let nombreSerpent = 3; nombreSerpent > 0; nombreSerpent--){

        let Case1 = CaseAleatoire();
        let Case2 = Case1 - DesAleatoire();

        let CouleurCourante = 0;
        let Couleur1 = 'Red';
        let Couleur2 = 'Orange';
        let Couleur3 = 'Green';

        if(nombreSerpent == 3){
            CouleurCourante = Couleur3;
        } else if(nombreSerpent == 2){
            CouleurCourante = Couleur2;
        } else {
            CouleurCourante = Couleur1;
        }

        while(Case1 < 2){
            Case1 = CaseAleatoire();
        }

        while(Case2 < 1){
            Case2 = Case1 - DesAleatoire();
        }

        if((Board[Case1] == '') && (Board[Case2] == '')){
            Board[Case1] = 'SD' + nombreSerpent;
            Board[Case2] = 'SF' + nombreSerpent;
            document.getElementById('C' + Case1).style.backgroundColor = CouleurCourante;
            document.getElementById('C' + Case2).style.backgroundColor = CouleurCourante;

        } else {
            nombreSerpent++;
            continue;
        }
    }
}

//Fonction utilisé pour vérifier si le joueur a gagner ou attéris sur un serpent ou échelle.
let Verification = function(){

    //Variable utilisé pour stocker l'index du joueur actif avant sont déplacement.
    let indexJoueur = 0;

    //Condition pour si un joueur a gagnant.
    if((indexP1 == 20) || (indexP2 == 20)){

        //Fais un annoncement selon le joueur gagnant.
        if(indexP1 == 20){
            Annoncer('Bravo au joueur #1, tu as gagné!\nAppuyer Commencer pour une nouvelle partie.');
        } else if(indexP2 == 20){
            Annoncer('Bravo au joueur #2, tu as gagné!\nAppuyer Commencer pour une nouvelle partie.');
        }

        reset();

    //Condition pour verifier si le joueur actif a l'index sur un serpent ou échelle. Si oui, ont affecte leurs position avec la Fin de celle-ci.
    } else if((Board[indexP1] == 'SD1') || (Board[indexP2] == 'SD1')){

        if(joueurActif == 'P1'){
            Annoncer('Le joueur #1 a tomber sur le serpent à la case #' + indexP1 + '!');
            EffacerImg(indexP1);
            indexP1 = Board.indexOf('SF1');
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        } else {
            Annoncer('Le joueur #2 a tomber sur le serpent à la case #' + indexP2 + '!');
            EffacerImg(indexP2);
            indexP2 = Board.indexOf('SF1');
            document.getElementById('C' + indexP2).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        }

    } else if((Board[indexP1] == 'SD2') || (Board[indexP2] == 'SD2')){

        if(joueurActif == 'P1'){
            Annoncer('Le joueur #1 a tomber sur le serpent à la case #' + indexP1 + '!');
            EffacerImg(indexP1);
            indexP1 = Board.indexOf('SF2');
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        } else {
            Annoncer('Le joueur #2 a tomber sur le serpent à la case #' + indexP2 + '!');
            EffacerImg(indexP2);
            indexP2 = Board.indexOf('SF2');
            document.getElementById('C' + indexP2).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        }

    } else if((Board[indexP1] == 'SD3') || (Board[indexP2] == 'SD3')){

        if(joueurActif == 'P1'){
            Annoncer('Le joueur #1 a tomber sur le serpent à la case #' + indexP1 + '!');
            EffacerImg(indexP1);
            indexP1 = Board.indexOf('SF3');
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        } else {
            Annoncer('Le joueur #2 a tomber sur le serpent à la case #' + indexP2 + '!');
            EffacerImg(indexP2);
            indexP2 = Board.indexOf('SF3');
            document.getElementById('C' + indexP2).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        }

    } else if((Board[indexP1] == 'ED1') || (Board[indexP2] == 'ED1')){

        if(joueurActif == 'P1'){
            Annoncer("Le joueur #1 a monté l'échelle à la case #" + indexP1 + '!');
            EffacerImg(indexP1);
            indexP1 = Board.indexOf('EF1');
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        } else {
            Annoncer("Le joueur #2 a monté l'échelle à la case #" + indexP2 + '!');
            EffacerImg(indexP2);
            indexP2 = Board.indexOf('EF1');
            document.getElementById('C' + indexP2).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        }

    } else if((Board[indexP1] == 'ED2') || (Board[indexP2] == 'ED2')){

        if(joueurActif == 'P1'){
            Annoncer("Le joueur #1 a monté l'échelle à la case #" + indexP1 + '!');
            EffacerImg(indexP1);
            indexP1 = Board.indexOf('EF2');
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        } else {
            Annoncer("Le joueur #2 a monté l'échelle à la case #" + indexP2 + '!');
            EffacerImg(indexP2);
            indexP2 = Board.indexOf('EF2');
            document.getElementById('C' + indexP2).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        }

    } else if((Board[indexP1] == 'ED3') || (Board[indexP2] == 'ED3')){

        if(joueurActif == 'P1'){
            Annoncer("Le joueur #1 a monté l'échelle à la case #" + indexP1 + '!');
            EffacerImg(indexP1);
            indexP1 = Board.indexOf('EF3');
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
        } else {
            Annoncer("Le joueur #2 a monté l'échelle à la case #" + indexP2 + '!');
            EffacerImg(indexP2);
            indexP2 = Board.indexOf('EF3');
            document.getElementById('C' + indexP2).innerHTML += "<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
        } 
    } else {
        //Sinon, fais la rotation des joueurs. 
        Annoncer('Rotation au prochain joueur!');
    }
}

//Fonction utilisé après avoir rouler le dés pour compléter sont déplacement. 
let AvancerJoueurActif = function(){
    
    //Variable utilisé pour stocker l'index du joueur actif avant sont déplacement.
    let indexJoueur = 0;

    //Condition selon le joueur actif 
    if(joueurActif == 'P1'){
        indexJoueur = indexP1;
        
        //Condition pour voir si le joueur dépasse la case #20 ou non. 
        if((indexP1 + nombreDes) > 20){
            Annoncer("Le joueur #1 doit atterir directement sur la case #20 pour gagner.\nRotation au prochain joueur!");
        } else if((indexP1 + nombreDes) <= 20){

            //Condition pour enlever l'image précedente si le joueur actif n'était pas sur la case 0 (ou il n'y a pas d'image par défaut).
            if(indexJoueur != 0){
                EffacerImg(indexJoueur);
            }

            indexP1 += nombreDes;
            document.getElementById('C' + indexP1).innerHTML += "<img src='img/fusee.png' alt='Piont du joueur #1' class='Piont' id='P1'>";
            Verification();
        }
    } else {
        indexJoueur = indexP2;

        if((indexP2 + nombreDes) > 20){
            Annoncer("Le joueur #2 doit atterir directement sur la case #20 pour gagner.\nRotation au prochain joueur!");
        } else if((indexP2 + nombreDes) <= 20){

            if(indexJoueur != 0){
                EffacerImg(indexJoueur);
            }

            indexP2 += nombreDes;
            document.getElementById('C' + indexP2).innerHTML +="<img src='img/diamand.png' alt='Piont du joueur #2' class='Piont' id='P2'>";
            Verification();
        }
    }
}